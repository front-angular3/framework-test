package stepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.Base_PO;
import pageobjects.Contact_Us_PO;

import static driver.DriverFactory.getDriver;

public class Contact_Us_Steps  extends Base_PO {
    private WebDriver driver = getDriver();

    private Contact_Us_PO contact_us_po;

    public Contact_Us_Steps(Contact_Us_PO contact_us_po){
        this.contact_us_po = contact_us_po;
    }

    @Given("I access the webdriver university contact us page")
    public void iAccessTheWebdriverUniversityContactUsPage() {
       contact_us_po.navigateTo_WebDriverUniversity_Contact_Us_Page();
    }

    @When("I enter a unique first name")
    public void iEnterAUniqueFirstName() {
        contact_us_po.setUnique_FirstName();
    }

    @And("I enter a unique last name")
    public void iEnterAUniqueLastName() {
        contact_us_po.setUnique_lastName();
    }

    @And("I enter a unique email address")
    public void iEnterAUniqueEmailAddress() {
        contact_us_po.setUnique_EmailAddress();
    }

    @And("I enter a unique comments")
    public void iEnterAUniqueComments() {
        contact_us_po.setUnique_comment();
    }

    @When("I enter a specific first name {word}")
    public void i_enter_a_specific_first_name_joe(String firstName) {
        contact_us_po.setSpecific_FirstName(firstName);
    }

    @When("I enter a specific last name {word}")
    public void i_enter_a_specific_last_name_blogs(String lastName) {
        contact_us_po.setSpecific_lastName(lastName);
    }

    @When("I enter a specific email address {word}")
    public void i_enter_a_specific_email_address(String email) {
        contact_us_po.setSpecific_emailAddress(email);
    }

    @When("I enter a specific comment {string}")
    public void i_enter_a_specific_comment(String comment) {
        contact_us_po.setUnique_comment();
    }


    @And("I click on the submit button")
    public void iClickOnTheSubmitBottom() {
        contact_us_po.clickOn_Submit_Button();
    }

    @Then("I  should be presented with a successful contact us submission message")
    public void iSIShouldBePresentedWithASuccessfulContactUsSubmissionMessage() {
        contact_us_po.validate_Successful_SubmissionMessage_Text();
    }

}
