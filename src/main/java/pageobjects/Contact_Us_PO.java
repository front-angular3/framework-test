package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import utils.Global_vars;

public class Contact_Us_PO extends Base_PO{

    private @FindBy(how = How.XPATH, using = "//input[@name='first_name']")
    WebElement firstName_TextField;

    private @FindBy(how = How.XPATH, using = "//input[@name='last_name']")
    WebElement lastName_TextField;

    private @FindBy(xpath = "//input[@name='email']")
    WebElement emailAddress_TextField;

    private @FindBy(xpath = "//textarea[@name='message']")
    WebElement comment_TextField;

    private @FindBy( xpath= "//input[@value='SUBMIT']")
    WebElement submit_Button;

    private @FindBy(xpath = "//div[@id='contact_reply']/h1")
    WebElement successfulSubmission_Message_Text;


    public Contact_Us_PO(){
        super();
    }

    public void navigateTo_WebDriverUniversity_Contact_Us_Page(){
        navigateTO_url(Global_vars.WEBDRIVER_UNIVERSITY_HOMEPAGE_URL + "/Contact-Us/contactus.html");
    }

    public void setUnique_FirstName(){
        sendkeys(firstName_TextField, "AutoFN" + generateRandomNumber(5));
    }

    public void setUnique_lastName(){
        sendkeys(lastName_TextField, "AutoLN" + generateRandomNumber(5));
    }

    public void setUnique_EmailAddress(){
        sendkeys(emailAddress_TextField, "AutoEmail" + generateRandomNumber(10)+ "@mail.com");
    }

    public void setUnique_comment(){
        sendkeys(comment_TextField, "Hello world " + generateRandomString(20));
    }

    public void setSpecific_FirstName(String firstName){
        sendkeys(firstName_TextField, firstName);
    }

    public void setSpecific_lastName(String lastName){
        sendkeys(lastName_TextField, lastName);
    }

    public void setSpecific_emailAddress(String emailAddress){
        sendkeys(emailAddress_TextField, emailAddress);
    }

    public void setSpecific_comment(String comment){
        sendkeys(comment_TextField, comment);
    }

    public void clickOn_Submit_Button(){
        waitForWebElementAndClick(submit_Button);
    }

    public void validate_Successful_SubmissionMessage_Text(){
        waitFor(successfulSubmission_Message_Text);
        Assert.assertEquals(successfulSubmission_Message_Text.getText(), "Thank You for your Message!");
    }
}